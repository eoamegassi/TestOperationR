package application;

import com.sun.javafx.robot.FXRobot;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.loadui.testfx.GuiTest;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class MainTest extends ApplicationTest
{

    TextField firstOp, secondOp, thirdOp;
    TextArea resultField;
    Button submitButton, lngButton;

    @Override
    public void start (Stage stage) throws Exception
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainScene.fxml"));
        Controller controller = new Controller();
        loader.setController(controller);

        Parent mainNode = loader.load();

        stage.setScene(new Scene(mainNode));
        stage.show();
        stage.toFront();
    }

    @Before
    public void setUp()
    {
        firstOp = GuiTest.find("#firstOp");
        secondOp = GuiTest.find("#secondOp");
        thirdOp = GuiTest.find("#thirdOp");

        resultField = GuiTest.find("#resultField");

        submitButton = GuiTest.find("#submitButton");
        lngButton = GuiTest.find("#lngButton");

        //menu = lookup("Menu").queryButton();

        clearInputs();
        //WaitForAsyncUtils.waitForFxEvents();
    }

    @After
    public void tearDown() throws Exception
    {
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @Test
    public void testDiscriminantPositif() throws Exception
    {
        setInputsAndSubmit("1", "0", "-1");

        assertEquals("L'equation devrait avoir 2 solutions: -1 et 1", "Les solutions sont x1: -1.0 x2: 1.0 S = {-1.0 , 1.0}", resultField.getText());
    }

    @Test
    public void testDiscriminantNul() throws Exception
    {
        setInputsAndSubmit("1", "2", "1");

        assertEquals("L'equation devrait avoir une solution double: -1", "Le système admet une solution double x0 :-1.0 S = {-1.0}", resultField.getText());
    }

    @Test
    public void testDiscriminantNegtif() throws Exception
    {
        setInputsAndSubmit("-1", "0", "-1");

        assertEquals("L'equation ne devrait pas avoir de solution dans IR", "Le système n'admet pas de solutions dans R", resultField.getText());

    }

    @Test
    public void testANul() throws Exception
    {
        setInputsAndSubmit("0", "2", "1");

        checkAlertMessage("la valeur de a ne doit pas être nulle");
    }

    @Test
    public void testChampsVides() throws Exception
    {
        setInputsAndSubmit("", "", "");

        checkAlertMessage("Veuiller saisir des nombres ou vérifier que tous les champs sont remplis");

    }

    @Test
    public void testInsertionCaractere() throws Exception
    {
        setInputsAndSubmit("a", "1s", "2");

        checkAlertMessage("Veuiller saisir des nombres ou vérifier que tous les champs sont remplis");
    }

    @Test
    public void testHistorique()
    {
        clickOn("Menu").moveTo("Historique").clickOn();
        interrupt();
        //WaitForAsyncUtils.waitForFxEvents();
    }

    public void clearInputs()
    {
        firstOp.clear();
        secondOp.clear();
        thirdOp.clear();

        interrupt();
    }

    public void setInputsAndSubmit(String a, String b, String c) throws Exception
    {
        clickOn(firstOp); write(a);
        clickOn(secondOp); write(b);
        clickOn(thirdOp); write(c);
        clickOn(submitButton);
        interrupt();
    }

    public Stage getTopModalStage()
    {
        // Get a list of windows but ordered from top[0] to bottom[n] ones.
        // It is needed to get the first found modal window.

        List<Window> allWindows = new ArrayList<>(listWindows());
        Collections.reverse(allWindows);

        return (Stage) allWindows
                .stream()
                .filter(window -> window instanceof Stage)
                .filter(window -> ((Stage) window).getModality() == Modality.APPLICATION_MODAL)
                .findFirst()
                .orElse(null);
    }

    public void checkAlertMessage(String messageAttendu)
    {
        Stage alertDialog = getTopModalStage();
        assertNotNull("Le dialogue ne doit pas etre null", alertDialog);

        DialogPane dialogPane = (DialogPane) alertDialog.getScene().getRoot();
        assertEquals("Le message devrait correspondre", messageAttendu, dialogPane.getContentText());
    }
}